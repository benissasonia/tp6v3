<?php

namespace App\Entity;

use App\Repository\DemoRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: DemoRepository::class)]
class Demo
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $demo = null;

    #[ORM\Column(length: 255)]
    private ?string $demo2 = null;

    #[ORM\Column(length: 255)]
    private ?string $demo3 = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDemo(): ?string
    {
        return $this->demo;
    }

    public function setDemo(string $demo): static
    {
        $this->demo = $demo;

        return $this;
    }

    public function getDemo2(): ?string
    {
        return $this->demo2;
    }

    public function setDemo2(string $demo2): static
    {
        $this->demo2 = $demo2;

        return $this;
    }

    public function getDemo3(): ?string
    {
        return $this->demo3;
    }

    public function setDemo3(string $demo3): static
    {
        $this->demo3 = $demo3;

        return $this;
    }
}
